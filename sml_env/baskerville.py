import requests
from sml.auth import auth_headers


def raise_or_return_json(method):
    def wrapped(*args, **kwargs):
        response = method(*args, **kwargs)
        response.raise_for_status()
        return response.json()
    return wrapped


class Session:
    def __init__(self, domain='sherlockml.com'):
        self.headers = auth_headers()
        self.base_url = 'https://baskerville.services.{}'.format(domain)

    @raise_or_return_json
    def get_environments(self, project_id):
        url = self.base_url + '/project/{project_id}/environment'.format(
            project_id=project_id
        )
        return requests.get(url, headers=self.headers)

    @raise_or_return_json
    def get_environment(self, project_id, environment_id):
        url = self.base_url + (
            '/project/{project_id}/environment/{environment_id}'
            ).format(
                project_id=project_id,
                environment_id=environment_id
            )
        return requests.get(url, headers=self.headers)

    @raise_or_return_json
    def put_environment(self, project_id, environment_id, payload):
        url = self.base_url + (
            '/project/{project_id}/environment/{environment_id}'
            ).format(
                project_id=project_id,
                environment_id=environment_id
            )
        return requests.put(url, headers=self.headers, json=payload)
