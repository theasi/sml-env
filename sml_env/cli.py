import json
import os
import sys

import click

from sml_env import baskerville
from sml_env.environment import resolve_environment
from sml_env.errors import ExceptionWithMessage


@click.group()
@click.option(
    '--project-id',
    type=str,
    help='SherlockML project ID',
    default=None,
    show_default=True
)
@click.option(
    '--domain',
    type=str,
    help='SherlockML domain',
    default='sherlockml.com',
    show_default=True
)
@click.pass_context
def cli(ctx, project_id, domain):
    """Manage custom environments on SherlockML."""
    # Resolve project
    if project_id is None:
        project_id = os.getenv('SHERLOCK_PROJECT_ID')
        if project_id is None:
            click.echo('Please specify --project-id after sml-sync.')
            sys.exit(1)
    ctx.obj['PROJECT_ID'] = project_id
    ctx.obj['SHERLOCKML_DOMAIN'] = domain


@cli.command()
@click.argument('requirements', type=click.Path(
    exists=True, resolve_path=True)
)
@click.argument('environment')
@click.option(
    '--manager',
    '-m',
    type=click.Choice(['pip', 'conda']),
    default='pip',
    help='package manager',
    show_default=True
)
@click.option(
    '--python-version',
    '-p',
    type=click.Choice(['2', '3', 'both']),
    default='both',
    help='Python version',
    show_default=True
)
@click.option(
    '--script',
    '-s',
    is_flag=True,
    help='push a script, ignoring -m and -p',
)
@click.pass_context
def push(
    ctx,
    requirements,
    environment,
    manager,
    python_version,
    script
):
    """
    Push REQUIREMENTS file (or a script) to a custom ENVIRONMENT.

    The section of the custom environment is specified using the --manager and
    --python-version options. Note that the specified section in the
    environment is overwritten with the file. If the flag --script is given,
    REQUIREMENTS is treated as a script and added to the scripts section of
    the custom environment.
    """
    project_id = ctx.obj['PROJECT_ID']
    domain = ctx.obj['SHERLOCKML_DOMAIN']
    baskerville_session = baskerville.Session(domain)

    requirements_path = click.format_filename(requirements)

    # Resolve environment
    try:
        target_environment = resolve_environment(
            baskerville_session,
            project_id,
            environment
        )
    except ExceptionWithMessage as e:
        click.echo(e.message)
        sys.exit(1)

    if script:
        # Treat file input as a script
        # TODO add 'option unused' warnings
        target_environment.add_script(requirements_path)
    else:
        # click.Choice does not like int with help:
        # https://github.com/pallets/click/issues/784
        if python_version == 'both':
            python_version = None
        else:
            python_version = int(python_version)
        # Parse requirements file and update environment
        target_environment.update_python(
            requirements_path,
            manager=manager,
            python_version=python_version
        )

    # Push
    baskerville_session.put_environment(
        project_id,
        target_environment.environment_id,
        target_environment.get_update_payload()
    )


@cli.command()
@click.argument('environment')
@click.argument('outfile', type=click.Path(resolve_path=True))
@click.pass_context
def export(
    ctx,
    environment,
    outfile
):
    """
    Export ENVIRONMENT as json to OUTFILE
    """
    project_id = ctx.obj['PROJECT_ID']
    domain = ctx.obj['SHERLOCKML_DOMAIN']
    baskerville_session = baskerville.Session(domain)

    # Resolve environment
    try:
        target_environment = resolve_environment(
            baskerville_session,
            project_id,
            environment
        )
    except ExceptionWithMessage as e:
        click.echo(e.message)
        sys.exit(1)

    with open(click.format_filename(outfile), 'w') as f:
        json.dump(target_environment.as_dict(), f)


def main():
    return cli(obj={})


if __name__ == "__main__":
    main()
