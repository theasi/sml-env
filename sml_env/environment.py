import uuid
from copy import deepcopy

from sml_env import requirements
from sml_env.errors import (AmbiguousNameError, EnvironmentNotFoundError,
                            InvalidPythonVersion)


class Environment:
    def __init__(self, name, specification, environment_id):
        self.name = name
        self.specification = specification
        self.environment_id = environment_id

    @classmethod
    def from_baskerville_response(cls, response):
        name = response['name']
        specification = deepcopy(response['specification'])
        # Removing the key is intentional (needed for baskerville put)
        environment_id = specification.pop('environmentId')
        return cls(name, specification, environment_id)

    def as_dict(self):
        response = {'name': self.name, 'specification': deepcopy(
            self.specification)
        }
        response['specification']['environmentId'] = self.environment_id
        return response

    def update_python(self, path, manager='pip', python_version=None):
        packages = requirements.parse(path, manager=manager)

        if python_version is None:
            python_versions = [2, 3]
        elif python_version == 2 or python_version == 3:
            python_versions = [python_version]
        else:
            raise InvalidPythonVersion

        for version in python_versions:
            self.specification['python'][
                'Python' + str(version)
            ][manager] = packages

    def add_script(self, path):
        with open(path, 'r') as f:
            script_contents = f.read()
        self.specification['bash'].append({'script': script_contents})

    def get_update_payload(self):
        return {'name': self.name, 'specification': self.specification}


def resolve_environment(baskerville_session, project_id, environment_spec):
    project_environments = [
        Environment.from_baskerville_response(environment) for
        environment in baskerville_session.get_environments(project_id)
    ]
    try:  # If input is a UUID
        target_environment_id = str(uuid.UUID(environment_spec))
        target_environment = None
        for possible_environment in project_environments:
            if possible_environment.environment_id == target_environment_id:
                target_environment = possible_environment  # Done
                break
        if target_environment is None:
            message = 'Environment with ID {} does not exist.'.format(
                target_environment_id
            )
            raise EnvironmentNotFoundError(message)
    except ValueError:  # Now do name resolution
        possible_environments = []
        for possible_environment in project_environments:
            if possible_environment.name == environment_spec:
                possible_environments.append(possible_environment)
        if len(possible_environments) > 1:
            message = 'Found more than one environment with the same name.'
            message += ' Please use the environment ID as input. IDs found:\n'
            message += '\n'.join(
                [environment.environment_id for
                    environment in possible_environments]
            )
            raise AmbiguousNameError(message)
        elif len(possible_environments) == 1:  # All good
            target_environment = possible_environments[0]  # Done
        else:  # len(environment_names) == 0
            message = 'Environment {} does not exist.'.format(
                environment_spec
            )
            raise EnvironmentNotFoundError(message)

    return target_environment
