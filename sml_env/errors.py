class InvalidPythonVersion(Exception):
    pass


class ExceptionWithMessage(Exception):
    def __init__(self, message):
        self.message = message


class EnvironmentNotFoundError(ExceptionWithMessage):
    pass


class AmbiguousNameError(ExceptionWithMessage):
    pass


class InvalidManager(Exception):
    pass


class UnsupportedOption(Exception):
    pass


class UnsupportedOperator(Exception):
    pass
