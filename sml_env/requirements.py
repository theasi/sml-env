import pathlib
from collections import OrderedDict

from sml_env.errors import (InvalidManager, UnsupportedOperator,
                            UnsupportedOption)

# Order matters for parsing
SUPPORTED_OPERATORS = OrderedDict({
    '===': False,
    '<=': True,
    '!=': False,
    '==': True,
    '>=': True,
    '~=': False,
    '<': True,
    '>': True
})


def _extract_constraint(package_spec):
    constraint = None
    for operator, supported in SUPPORTED_OPERATORS.items():
        if operator in package_spec:
            if not supported:
                raise UnsupportedOperator
            constraint = operator
            break
    return constraint


def _read_package_spec(package_spec):
    constraint = _extract_constraint(package_spec)
    if constraint is None:
        return {
            'name': package_spec, 'version': 'latest'
        }
    else:
        name, version = package_spec.split(constraint)
        return {
            'name': name,
            'version': {
                'constraint': constraint,
                'identifier': version
            }
        }


def parse(path, manager='pip'):
    if manager == 'conda':
        option_key = 'channels'
    elif manager == 'pip':
        option_key = 'extraIndexUrls'
    else:
        raise InvalidManager

    lines = pathlib.Path(path).read_text().split('\n')
    urls = []
    packages = []
    for line in lines:
        line = line.strip()
        # Comments or empty lines
        if line.startswith('#') or line == '':
            continue
        # Option
        if line.startswith('--'):
            if (
                line.startswith('--extra-index-url') and manager == 'pip'
            ) or (
                line.startswith('--channel') and manager == 'conda'
            ):
                urls.append(line.split('=')[1])
            else:
                raise UnsupportedOption
        # Requirement
        else:
            packages.append(_read_package_spec(line))

    return {option_key: urls, 'packages': packages}
