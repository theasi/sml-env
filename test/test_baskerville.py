#!/usr/bin/env python

import configparser
import unittest

from requests.exceptions import HTTPError

from sml_env import baskerville


class BaservilleTestCase(unittest.TestCase):
    def setUp(self):
        self.session = baskerville.Session()
        config = configparser.ConfigParser()
        config.read('input/baskerville.cfg')
        self.project_id = config['project']['id']
        self.environment_id = config['environment']['id']

    def tearDown(self):
        self.session = None

    def test_get_environment(self):
        self.assertEqual(
            self.session.get_environment(
                self.project_id, self.environment_id
            )['name'], 'test'
        )
        with self.assertRaises(HTTPError):
            self.session.get_environment(self.project_id, 'asi')


if __name__ == '__main__':
    unittest.main()
