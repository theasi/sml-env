import unittest

from test_baskerville import BaservilleTestCase
from test_requirements import RequirementsTestCase


def main():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(BaservilleTestCase))
    suite.addTest(unittest.makeSuite(RequirementsTestCase))
    runner = unittest.TextTestRunner()
    print(runner.run(suite))


if __name__ == "__main__":
    main()
