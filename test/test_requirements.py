#!/usr/bin/env python

import json
import unittest

from sml_env import requirements


class RequirementsTestCase(unittest.TestCase):
    def test_get_environment(self):
        with open('results/pip_parsed.json') as f:
            pip_results = json.load(f)
        with open('results/conda_parsed.json') as f:
            conda_results = json.load(f)
        self.assertEqual(
            requirements.parse('input/requirements.txt', manager='pip'),
            pip_results
        )
        self.assertEqual(
            requirements.parse(
                'input/conda-requirements.txt', manager='conda'),
            conda_results
        )


if __name__ == '__main__':
    unittest.main()
