from setuptools import find_packages, setup

setup(
    name='sml-env',
    version='0.0.1',
    description='SherlockML Custom Environments',
    url='https://www.asidatascience.com',
    author='ASI Data Science',
    author_email='engineering@asidatascience.com',
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        'requests',
        'sml>=0.9.0'
    ],
    entry_points={
        'console_scripts': ['sml-env=sml_env.cli:main'],
    }
)
