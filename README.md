# SherlockML Custom Environments CLI

`sml-env` is a command line tool for managing custom environments on
SherlockML.

To populate a custom environment with a `pip` `requirements.txt` file,

```bash
sml-env push requirements.txt dependencies
```

This assumes you're operating in the SherlockML project that contains the
environment called `dependencies`. From outside of the current project,
including your own computer, use the `--project-id` option after `sml-env`.

You can also export an environment to a JSON file.

```bash
sml-env export dependencies dependencies.json
```

More options are available, including support for `conda` files,
specifying which python versions to apply to and uploading scripts.

Use `sml-env --help` and `sml-env COMMAND --help` to find out more.

**Installation**

```bash
pip install -U git+https://www.bitbucket.org/theasi/sml-env.git
```

This assumes you have properly [configured `sml`](https://docs.sherlockml.com/advanced_usage/command_line_interface.html).

For more info, contact [setrak@asidatascience.com](mailto:setrak@asidatascience.com).
Pull requests are welcome.

Note: to run the tests, you need access to the SherlockML project named `sml-env`.
